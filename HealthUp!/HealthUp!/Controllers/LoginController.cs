﻿using HealthUp_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthUp_.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult LoginTela()
        {
            Session["TrocaSenha"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Logar(string email, string senha)
        {
            Login l = new Login();
            Session["TrocaSenha"] = null;
            if (l.Logar(email, senha))
            {

                if (l.TipoUsu(email, senha).ToString().Equals("Administrador"))
                {
                    Session["Type"] = "Administrador";
                    return RedirectToAction("Index", "Adm");
                }
                if (l.TipoUsu(email, senha).ToString().Equals("Nutricionista"))
                {
                    Session["EmailNutri"] = email;
                    Session["Type"] = "Nutricionista";
                    return RedirectToAction("Index", "Nutri");
                }

            }
            else
            {
                TempData["FalhaLogin"] = "Usuário ou senha incorretos";
            }

            return RedirectToAction("LoginTela", "Login");
        }

        public ActionResult Sair()
        {
            Session["aux"] = null;
            return RedirectToAction("LoginTela", "Login");
        }
    }
}