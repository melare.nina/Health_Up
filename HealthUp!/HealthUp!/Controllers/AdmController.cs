﻿using HealthUp_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace HealthUp_.Controllers
{
    public class AdmController : Controller
    {
        // GET: Adm
        public ActionResult Index()
        {
            Session["Lista"] = null;
            return View();
        }

        public ActionResult NutriTela()
        {
            Session["Lista"] = null;
            return View();
        }


        [HttpPost]
        public ActionResult NutriTela(string nome)
        {
            List<Nutri> lista = Nutri.ListaNutricionistas(nome);

            if (lista != null)
            {
                Session["Lista"] = "tem";
                return View(lista);
            }

            return RedirectToAction("NutriTela");
        }


        public ActionResult NutriPerfil(string id)
        {
            Session["Lista"] = null;
            Session["EmailNutri"] = id;
            Nutri n = Nutri.BuscaNutricionista(id);
            if (n == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir o perfil do nutricionista";
                return RedirectToAction("NutriTela");
            }
            return View(n);
        }



        public ActionResult EditarNutri(string id)
        {
            Session["Lista"] = null;

            Nutri n = Nutri.BuscaNutricionista(id);
            if (n == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir o perfil do nutricionista";
                return RedirectToAction("NutriTela");
            }

            return View(n);
        }

        [HttpPost]
        public ActionResult EditarNutri(string nome, string email, DateTime datanascimento, int crn, string telefone, string sexo)
        {
            Session["Lista"] = null;
            Nutri n = new Nutri();
            n.Nome = nome;
            n.Email = email;
            n.DataNasc = datanascimento;
            n.Telefone = telefone;
            n.Sexo = sexo;
            n.Crn = crn;
            TempData["Msg"] = n.Editar();
            Session["EmailNutri"] = email;

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("NutriPerfil", "Adm", new { email = email });
                }
                else return View();
            }
            else return View();
        }


        public ActionResult DeletarNutri(string email)
        {
            Session["Lista"] = null;
            Nutri n = new Nutri();
            n.Email = email;

            TempData["Msg"] = n.Excluir();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("NutriTela");
                }
                else return RedirectToAction("NutriPerfil", "Adm", new { email = email });
            }
            else return RedirectToAction("NutriPerfil", "Adm", new { email = email }); ;
        }

        public ActionResult CadastrarNutri()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastrarNutri(string nome, string email, DateTime datanascimento, int crn, string sexo, string telefone)
        {
            Session["Lista"] = null;
            Nutri n = new Nutri();
            n.Nome = nome;
            n.Email = email;
            n.DataNasc = datanascimento;
            n.Telefone = telefone;
            n.Sexo = sexo;
            n.Crn = crn;
            TempData["Msg"] = n.Cadastrar();
            Session["EmailPaciente"] = email;

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    
                        //Configurando a mensagem
                        MailMessage mail = new MailMessage();
                        //Origem
                        mail.From = new MailAddress("healthupnutricao@gmail.com");
                        //Destinatário
                        mail.To.Add(email);
                        //Assunto
                        mail.Subject = "Cadastro no HealthUp!";
                        //Corpo do e-mail
                        mail.Body = "Olá " + nome + "!\n\nVocê foi cadastrado no site de acompanhamento nutricional HEALTHUP!\n\nSeu usuário é o seu email: " + email + " e sua senha será: nutri123\n\nBem vindo!";

                        //Configurar o smtp
                        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                        //configurou porta
                        smtpServer.Port = 587;
                        //Habilitou o TLS
                        smtpServer.EnableSsl = true;
                        //Configurou usuario e senha p/ logar
                        smtpServer.Credentials = new System.Net.NetworkCredential("healthupnutricao@gmail.com", "senai123");

                        //Envia
                        smtpServer.Send(mail);

                    return RedirectToAction("NutriTela");
                }
                else return View();
            }
            else return View();
        }
        public ActionResult RecuperarSenhaEmail(string email)
        {
            Session["TrocaSenha"] = null;
            

            if (email != null)
            {
                Uri uri = new Uri("http://localhost:4502/Adm/RecuperarSenha");
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("healthupnutricao@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Recuperação de Senha no HealthUp!";
                //Corpo do e-mail
                mail.Body = "Olá!\n\nAqui está o link para a troca de senha: \n\n" + uri;

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("healthupnutricao@gmail.com", "senai123");

                //Envia
                smtpServer.Send(mail);

                return View(); ;
            }

            return RedirectToAction("LoginTela", "Login");
        }


        public ActionResult RecuperarSenha()
        {
            Session["TrocaSenha"] = null;
            return View();
           
        }

        [HttpPost]
        public ActionResult RecuperarSenha(string senha, string email)
        {
            Session["TrocaSenha"] = "trocado";
            Pessoa p = new Pessoa();
            p.Email = email;
            TempData["Msg"] = p.TrocaSenha(senha);

            return View();

    }
}
    }