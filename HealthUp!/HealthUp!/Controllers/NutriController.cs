﻿using HealthUp_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace HealthUp_.Controllers
{
    public class NutriController : Controller
    {
        // GET: Nutri
        public ActionResult Index()
        {
            Session["Lista"] = null;

            return View();
        }

        public ActionResult TabelasReferencias()
        {
            Session["Lista"] = null;
            return View();
        }

        public ActionResult TelaPaciente()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult TelaPaciente(string nome)
        {
            List<Pessoa> lista = Pessoa.ListaPaciente(nome);

            if (lista != null)
            {
                Session["Lista"] = "tem";
                return View(lista);
            }

            return RedirectToAction("TelaPaciente");
        }

        public ActionResult PacientePerfil(string id)
        {
            Session["Lista"] = null;
            Session["EmailPaciente"] = id;
            Pessoa p = Pessoa.BuscaPaciente(id);
            if (p == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir o perfil do paciente";
                return RedirectToAction("TelaPaciente");
            }
            return View(p);
        }
        public ActionResult TodasConsultas(string id)
        {
            Session["Lista"] = null;
            Session["EmailPaciente"] = id;
            Pessoa p = Pessoa.BuscaPaciente(id);
            if (p == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir as consultas do paciente";
                return RedirectToAction("PacientePerfil", "Nutri", new { email = id });
            }
            return View(p);
        }


        public ActionResult TodasDietas(string id)
        {
            Session["Lista"] = null;
            Session["EmailPaciente"] = id;
            List<Dieta> d = Dieta.ListaDietas(id);
            
            return View(d);
        }
        


        public ActionResult EditarPaciente(string id)
        {
            Session["Lista"] = null;

            Pessoa p = Pessoa.BuscaPaciente(id);
            if (p == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir o perfil do paciente";
                return RedirectToAction("TelaPaciente");
            }

            return View(p);
        }



        [HttpPost]
        public ActionResult EditarPaciente(string nome, string email, DateTime datanascimento, string telefone, string sexo)
        {
            Session["Lista"] = null;
            Pessoa p = new Pessoa();
            p.Nome = nome;
            p.Email = email;
            p.DataNasc = datanascimento;
            p.Telefone = telefone;
            p.Sexo = sexo;
            TempData["Msg"] = p.Editar();
            Session["EmailPaciente"] = email;

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("PacientePerfil", "Nutri", new { id = email + "/" });
                }
                else return View();
            }
            else return View();
        }


        public ActionResult DeletarPaciente(string email)
        {
            Session["Lista"] = null;
            Pessoa p = new Pessoa();
            p.Email = email;

            TempData["Msg"] = p.Excluir();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("TelaPaciente");
                }
                else return RedirectToAction("PacientePerfil", "Nutri", new { email = email });
            }
            else return RedirectToAction("PacientePerfil", "Nutri", new { email = email }); ;
        }

        public ActionResult CadastrarPaciente()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastrarPaciente(string nome, string email, DateTime datanascimento, string sexo, string telefone)
        {
            Session["Lista"] = null;
            Pessoa p = new Pessoa();
            p.Nome = nome;
            p.Email = email;
            p.DataNasc = datanascimento;
            p.Telefone = telefone;
            p.Sexo = sexo;
            TempData["Msg"] = p.Cadastrar();
            Session["EmailPaciente"] = email;

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    
                    //Configurando a mensagem
                    MailMessage mail = new MailMessage();
                    //Origem
                    mail.From = new MailAddress("healthupnutricao@gmail.com");
                    //Destinatário
                    mail.To.Add(email);
                    //Assunto
                    mail.Subject = "Cadastro no HealthUp!";
                    //Corpo do e-mail
                    mail.Body = "Olá " + nome + "!\n\nVocê foi cadastrado no site de acompanhamento nutricional HEALTHUP!\n\nSeu usuário é o seu email: " + email + " e sua senha será: paciente123\n\nBem vindo!";

                    //Configurar o smtp
                    SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                    //configurou porta
                    smtpServer.Port = 587;
                    //Habilitou o TLS
                    smtpServer.EnableSsl = true;
                    //Configurou usuario e senha p/ logar
                    smtpServer.Credentials = new System.Net.NetworkCredential("healthupnutricao@gmail.com", "senai123");

                    //Envia
                    smtpServer.Send(mail);

                    return RedirectToAction("Questionario");
                }
                else return View();
            }
            else return View();
        }


        /*---------------------------------------------------------------QUESTIONÁRIO----------------------------------------------------------*/
        public ActionResult Questionario()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Questionario(string digestao, string apetite, string estresse, string dormeBem, string fumante, string ansiedade, string insonia,
            string ingereAlcool, string doresAbdominais, string azia, string horaAzia, string liquidoRefeicao, string qtdLiquido, string suplementos,
            string qtdSuplemento, string qtdAgua, string horaMaiorApetite, string horaMenorApetite, string dormeHrs, string acordaHrs, string problemasSaude,
            string dietaEspecial, string localRefeicao, string intolerancia, string preferenciaAlimentos, string vicios, string cafe, string almoco, string janta)
        {
            Session["Lista"] = null;
            Questionario q = new Questionario();

            q.Digestao = digestao;
            q.Estresse = estresse;
            q.Apetite = apetite;
            q.DormeBem = dormeBem;
            q.Fumante = fumante;
            q.Ansiedade = ansiedade;
            q.Insonia = insonia;
            q.IngereAlcool = ingereAlcool;
            q.DoresAbdomem = doresAbdominais;
            q.Azia = azia;
            q.HrAzia = horaAzia;
            q.LiquidoRefeicao = liquidoRefeicao;
            q.Suplementos = suplementos;
            q.QtdSuplementos = float.Parse(qtdSuplemento);
            q.QtdAgua = float.Parse(qtdAgua);
            q.HrMaiorApetite = horaMaiorApetite;
            q.HrMenorApetite = horaMenorApetite;
            q.DormeHrs = dormeHrs;
            q.AcordaHrs = acordaHrs;
            q.ProblemaSaude = problemasSaude;
            q.DietaEspecial = dietaEspecial;
            q.LocalRefeicao = localRefeicao;
            q.Intolerancia = intolerancia;
            q.Preferencia = preferenciaAlimentos;
            q.Vicios = vicios;
            q.Cafe = cafe;
            q.Almoco = almoco;
            q.Janta = janta;
            
            if(qtdLiquido == "")
            {
                q.QtdLiquido = 0;
            }
            else
            {
                q.QtdLiquido = float.Parse(qtdLiquido);
            }
            


            q.EmailNutri = Session["EmailNutri"].ToString();
            q.EmailPaciente = Session["EmailPaciente"].ToString();



            TempData["Msg"] = q.CadastrarQuestionario();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("TelaPaciente");
                }
                else return View();
            }
            else return View();
        }

        /*---------------------------------------------------------------QUESTIONÁRIO----------------------------------------------------------*/


        /*------------------------------------------------------------------CONSULTA------------------------------------------------------------*/
        public ActionResult Consulta()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Consulta(double peso, double altura, double porcentGorduraCorpo, double massaMagra,
        double porcentMuscEsque, double porcentGorduraVisc, string metabolismo, double abdominal, 
        double coxaEsq, double coxaDir, double triceps, double ombro, double biceps, double cintura, double quadril,
        double pescoco, double punho, double antebracoDir, double antebracoEsq, double subescapular, double torax,
        double panturrilhaEsq, double panturrilhaDir, double peitoral)
        {
            Session["Lista"] = null;
            Consulta c = new Consulta();

            c.Data = DateTime.Today;
            c.PorcentGorduraCorpo = porcentGorduraCorpo;
            c.MassaMagra = massaMagra;
            c.Altura = altura;
            c.Peso = peso;
            c.PorcentGorduraVisce = porcentGorduraVisc;
            c.Metabolismo = metabolismo;
            c.Abdominal = abdominal;
            c.Biceps = biceps;
            c.CoxaEsq = coxaEsq;
            c.CoxaDir = coxaDir;
            c.PanturrilhaEsq = panturrilhaEsq;
            c.PanturrilhaDir = panturrilhaDir;
            c.Subescapular = subescapular;
            c.PorcentMuscEsque = porcentMuscEsque;
            c.Torax = torax;
            c.Triceps = triceps;
            c.Ombro = ombro;
            c.Peitoral = peitoral;
            c.Cintura = cintura;
            c.Quadril = quadril;
            c.Pescoco = pescoco;
            c.Punho = punho;
            c.AntebracoDir = antebracoDir;
            c.AntebracoEsq = antebracoEsq;
            c.EmailNutri = Session["EmailNutri"].ToString();
            c.EmailPaciente = Session["EmailPaciente"].ToString();

            TempData["Msg"] = c.CadastrarConsulta();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("TelaPaciente");
                }
                else return View();
            }
            else return View();
        }
        
        /*------------------------------------------------------------------CONSULTA------------------------------------------------------------*/


        /*------------------------------------------------------------------ALIMENTOS------------------------------------------------------------*/
        public ActionResult AlimentosTela()
        {
            Session["ListaAlimento"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult AlimentosTela(string nome)
        {
            List<Alimentos> lista = Alimentos.ListaAlimentos(nome);

            if (lista != null)
            {
                Session["ListaAlimento"] = "tem";
                return View(lista);
            }

            return RedirectToAction("AlimentosTela");
        }


        public ActionResult EditarAlimento(string nome)
        {
            Session["Lista"] = null;
            Session["NomeAlimento"] = nome;
            Alimentos a = Alimentos.BuscaAlimento(nome);

            if (a == null)
            {
                TempData["Msg"] = "Ocorreu um erro ao abrir o perfil do alimento";
                return RedirectToAction("AlimentosTela");
            }
            return View(a);
        }

        [HttpPost]
        public ActionResult EditarAlimento(string nome, string proteina, string calcio, string gordura, string kcal)
        {
            Session["ListaAlimento"] = null;
            Alimentos a = new Alimentos();
            a.Nome = nome;
            a.Proteina = double.Parse(proteina);
            a.Calcio = double.Parse(calcio);
            a.Gordura = double.Parse(gordura);
            a.Kcal = double.Parse(kcal);

            TempData["Msg"] = a.Editar(Session["NomeAlimento"].ToString());

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("AlimentosTela");
                }
                else return View();
            }
            else return View();
        }


        public ActionResult ExcluirAlimento(string nome)
        {
            Session["Lista"] = null;
            Alimentos a = new Alimentos();
            a.Nome = nome;

            TempData["Msg"] = a.Excluir();
            
            return RedirectToAction("AlimentosTela"); ;
        }

        public ActionResult CadastrarAlimento()
        {
            Session["ListaAlimento"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastrarAlimento(string nome, string proteina, string calcio, string gordura, string kcal)
        {
            Session["ListaAlimento"] = null;
            Alimentos a = new Alimentos();
            a.Nome = nome;
            a.Proteina = double.Parse(proteina);
            a.Calcio = double.Parse(calcio);
            a.Gordura = double.Parse(gordura);
            a.Kcal = double.Parse(kcal);
            TempData["Msg"] = a.CadastrarAlimento();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("AlimentosTela");
                }
                else return View();
            }
            else return View();
        }
        /*------------------------------------------------------------------ALIMENTOS------------------------------------------------------------*/





        /*------------------------------------------------------------------DIETA------------------------------------------------------------*/
        public ActionResult GerarDieta(string id)
        {
            Session["Lista"] = null;
            Session["EmailDieta"] = id;
            string nome = "";

            List<Alimentos> alimentos = Alimentos.ListaAlimentos(nome);

            return View(alimentos);
        }

        [HttpPost]
        public ActionResult GerarDieta(int alimento1, int qtd1, int alimento2, int qtd2, int alimento3,
            int qtd3, int alimento4, int qtd4, int alimento1A, int qtd1a, int alimento2A, int qtd2a,
            int alimento3A, int qtd3a, int alimento4A, int qtd4a, int alimento1J, int qtd1j, int alimento2J,
            int qtd2j, int alimento3J, int qtd3j, int alimento4J, int qtd4j)
        {
            string horarioCafe = "09:00", horarioAlmoco = "13:00", horarioJanta = "19:00";

            Dieta d = new Dieta();

            d.EmailNutri = Session["EmailNutri"].ToString();
            d.EmailPaciente = Session["EmailDieta"].ToString();

            //Passando os horários pré definidos
            for (int i = 0; i <=3; i++)
            {
                d.HorarioAlimento[i] = horarioCafe;
            }

            for (int i = 4; i <= 7; i++)
            {
                d.HorarioAlimento[i] = horarioAlmoco;
            }

            for (int i = 8; i <= 11; i++)
            {
                d.HorarioAlimento[i] = horarioJanta;
            }

            //Passando os Alimentos                 //Passando sua quantidade
            d.IdAlimento[0] = alimento1;            d.QuantidadeAlimento[0] = qtd1;
            d.IdAlimento[1] = alimento2;            d.QuantidadeAlimento[1] = qtd2;
            d.IdAlimento[2] = alimento3;            d.QuantidadeAlimento[2] = qtd3;
            d.IdAlimento[3] = alimento4;            d.QuantidadeAlimento[3] = qtd4;
            d.IdAlimento[4] = alimento1A;           d.QuantidadeAlimento[4] = qtd1a;
            d.IdAlimento[5] = alimento2A;           d.QuantidadeAlimento[5] = qtd2a;
            d.IdAlimento[6] = alimento3A;           d.QuantidadeAlimento[6] = qtd3a;
            d.IdAlimento[7] = alimento4A;           d.QuantidadeAlimento[7] = qtd4a;
            d.IdAlimento[8] = alimento1J;           d.QuantidadeAlimento[8] = qtd1j;
            d.IdAlimento[9] = alimento2J;           d.QuantidadeAlimento[9] = qtd2j;
            d.IdAlimento[10] = alimento3J;          d.QuantidadeAlimento[10] = qtd3j;
            d.IdAlimento[11] = alimento4J;          d.QuantidadeAlimento[11] = qtd4j;

            TempData["Msg"] = d.CadastrarDieta();

            if (TempData["Msg"] != null)
            {
                if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
                {
                    return RedirectToAction("PacientePerfil", "Nutri", new { email = d.EmailPaciente });
                }
                else return View();
            }
            else return View();
        }


        private GerarPDF GerarRelPDF(int numDieta, string emailPaciente, string emailNutri)
        {

            var gp = new GerarPDF(numDieta, emailNutri, emailPaciente);

            Pessoa p = Pessoa.BuscaPaciente(emailPaciente);
            Nutri n = Nutri.BuscaNutricionista(emailNutri);

            gp.PageTitle = "Dieta de número " + numDieta + " do(a) paciente " + p.Nome + "\n\nGerada pelo(a) nutricionista " + n.Nome;
            gp.PageTitle = "Dieta de número " + numDieta + " do(a) paciente " + p.Nome + "\n\nGerada pelo(a) nutricionista "+ n.Nome;
            gp.ImprimirCabecalhoPadrao = true;
            gp.ImprimirCabecalhoPadrao = true;

            return gp;
        }

        public ActionResult Preview(int numDieta, string emailPaciente, string emailNutri)
        {
            
            var gp = GerarRelPDF(numDieta, emailPaciente, emailNutri);

            return File(gp.GetOutput().GetBuffer(), "Dieta/pdf");
        }


        private GerarPDFConsultas GerarRelPDFConsulta(string emailPaciente, string emailNutri, string dataConsulta)
        {

            var gp = new GerarPDFConsultas(emailNutri, emailPaciente, dataConsulta);

            Pessoa p = Pessoa.BuscaPaciente(emailPaciente);
            Nutri n = Nutri.BuscaNutricionista(emailNutri);

            gp.PageTitle = "Consulta do Dia " + dataConsulta.Remove(11,8) + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
            gp.PageTitle = "Consulta do Dia " + dataConsulta.Remove(11, 8) + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
            gp.ImprimirCabecalhoPadrao = true;
            gp.ImprimirCabecalhoPadrao = true;

            return gp;
        }

        public ActionResult PreviewConsulta(string emailPaciente, string emailNutri, string dataConsulta)
        {

            var gp = GerarRelPDFConsulta(emailPaciente, emailNutri, dataConsulta);

            return File(gp.GetOutput().GetBuffer(), "Consulta/pdf");
        }
        /*------------------------------------------------------------------DIETA------------------------------------------------------------*/
    }
}