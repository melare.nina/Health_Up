﻿using HealthUp_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace HealthUp_
{
    /// <summary>
    /// Summary description for WebServiceHealthUp
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceHealthUp : System.Web.Services.WebService
    {

        [WebMethod]
        public string Logar(string email, string senha)
        {
            Login l = new Login();

            if (l.Logar(email, senha))
            {
                if (l.TipoUsu(email, senha).ToString().Equals("Administrador"))
                {
                    return "Administrador";
                }
                if (l.TipoUsu(email, senha).ToString().Equals("Nutricionista"))
                {
                    return "Nutricionista";
                }
                if (l.TipoUsu(email, senha).ToString().Equals("Paciente"))
                {
                    return "Paciente";
                }
            }

            return "Erro";
        }


        [WebMethod]
        public List<Consulta> ListaConsultas(string email)
        {
            Pessoa p = new Pessoa();

            p = Pessoa.BuscaPaciente(email);

            List<Consulta> lista = p.Consultas;

            return lista;
        }

        [WebMethod]
        public List<Dieta> ListaDietas(string email)
        {
            List<Dieta> lista = Dieta.ListaDietas(email);
        
            return lista;
        }

        

        private GerarPDF GerarRelPDF(int numDieta, string emailPaciente, string emailNutri)
        {

            var gp = new GerarPDF(numDieta, emailNutri, emailPaciente);

            Pessoa p = Pessoa.BuscaPaciente(emailPaciente);
            Nutri n = Nutri.BuscaNutricionista(emailNutri);

            gp.PageTitle = "Dieta de número " + numDieta + " do(a) paciente " + p.Nome + "\n\nGerada pelo(a) nutricionista " + n.Nome;
            gp.PageTitle = "Dieta de número " + numDieta + " do(a) paciente " + p.Nome + "\n\nGerada pelo(a) nutricionista " + n.Nome;
            gp.ImprimirCabecalhoPadrao = true;
            gp.ImprimirCabecalhoPadrao = true;

            return gp;
        }

        [WebMethod]
        public void Preview(int numDieta, string emailPaciente, string emailNutri)
        {
            var gp = GerarRelPDF(numDieta, emailPaciente, emailNutri);

            HttpResponse response = HttpContext.Current.Response;

            //response.Clear();
            //response.ContentType = "application/octet-stream";
            //response.AddHeader("content-disposition", string.Format("attachment; filename=\"{0}\"", "dieta.pdf"));
            //response.Flush();
            //response.BinaryWrite(gp.GetOutput().GetBuffer());
            //response.Flush();
            //response.End();
            

            Context.Response.ContentType = "text/plain";
            Byte[] bytes = gp.GetOutput().GetBuffer();
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            Context.Response.Write(base64String);
        }


        private GerarPDFConsultas GerarRelPDFConsulta(string emailPaciente, string emailNutri, string dataConsulta)
        {

            var gp = new GerarPDFConsultas(emailNutri, emailPaciente, dataConsulta);

            Pessoa p = Pessoa.BuscaPaciente(emailPaciente);
            Nutri n = Nutri.BuscaNutricionista(emailNutri);
            if (dataConsulta.Length >= 11)
            {
                gp.PageTitle = "Consulta do Dia " + dataConsulta.Remove(11, 8) + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
                gp.PageTitle = "Consulta do Dia " + dataConsulta.Remove(11, 8) + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
            }
            else
            {
                gp.PageTitle = "Consulta do Dia " + dataConsulta + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
                gp.PageTitle = "Consulta do Dia " + dataConsulta + " do(a) paciente " + p.Nome + "\n\nRealizada pelo(a) nutricionista " + n.Nome;
            }
            
            gp.ImprimirCabecalhoPadrao = true;
            gp.ImprimirCabecalhoPadrao = true;

            return gp;
        }

        [WebMethod]
        public void PreviewConsulta(string emailPaciente, string emailNutri, string dataConsulta)
        {

            var gp = GerarRelPDFConsulta(emailPaciente, emailNutri, dataConsulta);

            HttpResponse response = HttpContext.Current.Response;

            //response.Clear();
            //response.ContentType = "application/octet-stream";
            //response.AddHeader("content-disposition", string.Format("attachment; filename=\"{0}\"", "consulta.pdf"));
            //response.Flush();
            //Context.Response.BinaryWrite(gp.GetOutput().GetBuffer());
            //response.BinaryWrite(gp.GetOutput().GetBuffer());
            //response.Flush();
            //response.End();


            Context.Response.ContentType = "text/plain";
            Byte[] bytes = gp.GetOutput().GetBuffer();
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            Context.Response.Write(base64String);
            
        }

    }
}
