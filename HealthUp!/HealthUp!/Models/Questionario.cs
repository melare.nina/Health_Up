﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Questionario
    {
        string localRefeicao, estresse, digestao, dormeBem, fumante, ansiedade, insonia;
        string liquidoRefeicao, intolerancia, preferencia, almoco, vicios, ingereAlcool;
        string problemasSaude, doresAbdomem, janta, dietaEspecial, dormeHrs, acordaHrs;
        string suplementos, cafe, hrMaiorApetite, hrMenorApetite;
        string azia, apetite, hrAzia, emailNutri, emailPaciente;
        float qtdSuplementos, qtdLiquido, qtdAgua;

        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public string LocalRefeicao
        {
            get { return localRefeicao; }
            set
            { localRefeicao = value; }
        }

        public string Estresse
        {
            get { return estresse; }
            set { estresse = value; }
        }

        public string Digestao
        {
            get { return digestao; }
            set { digestao = value; }
        }

        public string DormeBem
        {
            get { return dormeBem; }
            set { dormeBem = value; }
        }

        public string Fumante
        {
            get { return fumante; }
            set { fumante = value; }
        }

        public string Ansiedade
        {
            get { return ansiedade; }
            set { ansiedade = value; }
        }

        public string Insonia
        {
            get { return insonia; }
            set { insonia = value; }
        }

        public string LiquidoRefeicao
        {
            get { return liquidoRefeicao; }
            set
            { liquidoRefeicao = value; }
        }

        public string Intolerancia
        {
            get { return intolerancia; }
            set { intolerancia = value; }
        }

        public string Preferencia
        {
            get { return preferencia; }
            set { preferencia = value; }
        }

        public string Almoco
        {
            get { return almoco; }
            set { almoco = value; }
        }

        public string Vicios
        {
            get { return vicios; }
            set { vicios = value; }
        }

        public string IngereAlcool
        {
            get { return ingereAlcool; }
            set { ingereAlcool = value; }
        }

        public string ProblemaSaude
        {
            get { return problemasSaude; }
            set { problemasSaude = value; }
        }

        public string DoresAbdomem
        {
            get { return doresAbdomem; }
            set
            { doresAbdomem = value; }
        }

        public string Janta
        {
            get { return janta; }
            set { janta = value; }
        }

        public string DietaEspecial
        {
            get { return dietaEspecial; }
            set { dietaEspecial = value; }
        }

        public string DormeHrs
        {
            get { return dormeHrs; }
            set { dormeHrs = value; }
        }

        public string AcordaHrs
        {
            get { return acordaHrs; }
            set { acordaHrs = value; }
        }

        public string Suplementos
        {
            get { return suplementos; }
            set { suplementos = value; }
        }

        public string Cafe
        {
            get { return cafe; }
            set { cafe = value; }
        }

        public string HrMaiorApetite
        {
            get { return hrMaiorApetite; }
            set
            { hrMaiorApetite = value; }
        }

        public string HrMenorApetite
        {
            get { return hrMenorApetite; }
            set { hrMenorApetite = value; }
        }

        public string Azia
        {
            get { return azia; }
            set { azia = value; }
        }

        public string Apetite
        {
            get { return apetite; }
            set { apetite = value; }
        }

        public string HrAzia
        {
            get { return hrAzia; }
            set { hrAzia = value; }
        }

        public string EmailNutri
        {
            get { return emailNutri; }
            set { emailNutri = value; }
        }

        public string EmailPaciente
        {
            get { return emailPaciente; }
            set { emailPaciente = value; }
        }

        public float QtdSuplementos
        {
            get { return qtdSuplementos; }
            set { qtdSuplementos = value; }
        }

        public float QtdLiquido
        {
            get { return qtdLiquido; }
            set { qtdLiquido = value; }
        }

        public float QtdAgua
        {
            get { return qtdAgua; }
            set { qtdAgua = value; }
        }

        public string CadastrarQuestionario()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Questionario VALUES(@LocalRefeicao, @Estresse,  @Digestao, @DormeBem, @Fumante,"+
                    "@Ansiedade, @Insonia, @LiquidoRefeicao,  @Intolerancia, @PreferenciaAlimentos, @Almoco, @Vicios, @IngereAlcool, @ProblemasSaude,"+
                    "@DoresAbdominais, @Janta, @DietaEspecial, @DormeHrs, @AcordaHrs, @Suplementos,  @QtdSuplementos, @Cafe, @QtdAguaDia, @HoraMaiorApetite,"+
                    "@HoraMenorApetite, @QtdLiquido, @Azia,  @Apetite, @HorarioAzia, @EmailNutri, @EmailPaciente)", con);
            
                query.Parameters.AddWithValue("@LocalRefeicao", localRefeicao);
                query.Parameters.AddWithValue("@Estresse", estresse);
                query.Parameters.AddWithValue("@Digestao", digestao);
                query.Parameters.AddWithValue("@DormeBem", dormeBem);
                query.Parameters.AddWithValue("@Fumante", fumante);
                query.Parameters.AddWithValue("@Ansiedade", ansiedade);
                query.Parameters.AddWithValue("@Insonia", insonia);
                query.Parameters.AddWithValue("@LiquidoRefeicao", liquidoRefeicao);
                query.Parameters.AddWithValue("@Intolerancia", intolerancia);
                query.Parameters.AddWithValue("@PreferenciaAlimentos", preferencia);
                query.Parameters.AddWithValue("@Almoco", almoco);
                query.Parameters.AddWithValue("@Vicios", vicios);
                query.Parameters.AddWithValue("@IngereAlcool", ingereAlcool);
                query.Parameters.AddWithValue("@ProblemasSaude", problemasSaude);
                query.Parameters.AddWithValue("@DoresAbdominais", doresAbdomem);
                query.Parameters.AddWithValue("@Janta", janta);
                query.Parameters.AddWithValue("@DietaEspecial", dietaEspecial);
                query.Parameters.AddWithValue("@DormeHrs", dormeHrs);
                query.Parameters.AddWithValue("@AcordaHrs", acordaHrs);
                query.Parameters.AddWithValue("@Suplementos", suplementos);
                query.Parameters.AddWithValue("@QtdSuplementos", qtdSuplementos);
                query.Parameters.AddWithValue("@Cafe", cafe);
                query.Parameters.AddWithValue("@QtdAguaDia", qtdAgua);
                query.Parameters.AddWithValue("@HoraMaiorApetite", hrMaiorApetite);
                query.Parameters.AddWithValue("@HoraMenorApetite", hrMenorApetite);
                query.Parameters.AddWithValue("@QtdLiquido", qtdLiquido);
                query.Parameters.AddWithValue("@Azia", azia);
                query.Parameters.AddWithValue("@Apetite", apetite);
                query.Parameters.AddWithValue("@HorarioAzia", hrAzia);
                query.Parameters.AddWithValue("@EmailNutri", emailNutri);
                query.Parameters.AddWithValue("@EmailPaciente", emailPaciente);
                query.ExecuteNonQuery();
                
            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Qustionário cadastrado com sucesso!";
        }



    }
}