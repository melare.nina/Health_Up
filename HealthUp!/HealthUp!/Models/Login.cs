﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Login
    {
        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        

        private string email;
        private string senha;
        string res;
        
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }


        public Boolean Logar(string email, string senha)
        {
            Boolean logado = false;
            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa WHERE Email = @Email AND Senha = @Senha", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Senha", senha);
                //Pegando a tabela, mandando executar e jogando ela na minha variável do tipo DataReader
                SqlDataReader leitor = query.ExecuteReader();

                logado = leitor.HasRows;
            }
            catch (Exception ee)
            {
                logado = false;
            }

            con.Close();
            return logado;

        }

        public string TipoUsu(string email, string senha)
        {

            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa WHERE Email = @Email AND Senha = @Senha", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Senha", senha);
                //Pegando a tabela, mandando executar e jogando ela na minha variável do tipo DataReader
                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    //Enquanto tiver linhas para ler

                    while (leitor.Read())
                    {
                        res = leitor["TipoUsuario"].ToString();
                    }

                }
                con.Close();

            }
            catch (Exception ee)
            {
                return ee.Message;
            }
            return res;
           
        }
    }
}