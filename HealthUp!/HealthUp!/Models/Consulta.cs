﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Consulta
    {
        double imc, peso, porcentGorduraCorpo, massaMagra, altura, perdas, porcentMuscEsque, pesoIdeal, PorcentGorduraVisc, abdominal, biceps, coxaEsq, coxaDir, panturrilhaEsq, panturrilhaDir, subescapular, torax, triceps, ombro, peitoral, cintura, quadril, pescoco, punho, antebracoDir, antebracoEsq, progresso;
        string emailNutri, metabolismo, emailPaciente;
        DateTime data;

        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);



        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public string EmailNutri
        {
            get { return emailNutri; }
            set { emailNutri = value; }
        }
        public string EmailPaciente
        {
            get { return emailPaciente; }
            set { emailPaciente = value; }
        }

        public double Imc
        {
            get { return imc; }
            set { imc = value; }
        }
        public double Peso
        {
            get { return peso; }
            set { peso = value; }
        }

        public double PorcentGorduraCorpo
        {
            get { return porcentGorduraCorpo; }
            set { porcentGorduraCorpo = value; }
        }
        public double MassaMagra
        {
            get { return massaMagra; }
            set { massaMagra = value; }
        }
        public double Altura
        {
            get { return altura; }
            set { altura = value; }
        }
        public double Perdas
        {
            get { return perdas; }
            set { perdas = value; }
        }
        public double PorcentMuscEsque
        {
            get { return porcentMuscEsque; }
            set { porcentMuscEsque = value; }
        }
        public double PesoIdeal
        {
            get { return pesoIdeal; }
            set { pesoIdeal = value; }
        }
        virtual public double PorcentGorduraVisce
        {
            get { return PorcentGorduraVisc; }
            set { PorcentGorduraVisc = value; }
        }

        public string Metabolismo
        {
            get { return metabolismo; }
            set { metabolismo = value; }
        }
        public double Abdominal
        {
            get { return abdominal; }
            set { abdominal = value; }
        }
        public double Biceps
        {
            get { return biceps; }
            set { biceps = value; }
        }
        public double CoxaEsq
        {
            get { return coxaEsq; }
            set { coxaEsq = value; }
        }
        public double CoxaDir
        {
            get { return coxaDir; }
            set { coxaDir = value; }
        }
        public double PanturrilhaEsq
        {
            get { return panturrilhaEsq; }
            set { panturrilhaEsq = value; }
        }
        public double PanturrilhaDir
        {
            get { return panturrilhaDir; }
            set { panturrilhaDir = value; }
        }
        public double Subescapular
        {
            get { return subescapular; }
            set { subescapular = value; }
        }
        public double Torax
        {
            get { return torax; }
            set { torax = value; }
        }
        public double Triceps
        {
            get { return triceps; }
            set { triceps = value; }
        }
        public double Ombro
        {
            get { return ombro; }
            set { ombro = value; }
        }
        public double Peitoral
        {
            get { return peitoral; }
            set { peitoral = value; }
        }
        public double Cintura
        {
            get { return cintura; }
            set { cintura = value; }
        }
        public double Quadril
        {
            get { return quadril; }
            set { quadril = value; }
        }
        public double Pescoco
        {
            get { return pescoco; }
            set { pescoco = value; }
        }
        public double Punho
        {
            get { return punho; }
            set { punho = value; }
        }
        public double AntebracoDir
        {
            get { return antebracoDir; }
            set { antebracoDir = value; }
        }
        public double AntebracoEsq
        {
            get { return antebracoEsq; }
            set { antebracoEsq = value; }
        }
        public double Progresso
        {
            get { return progresso; }
            set { progresso = value; }
        }



        public string CadastrarConsulta()
        {

            try
            {
                //Calculando o IMC do paciente e jogando na variável imc para poder jogar no banco depois
                imc = IMC(altura, peso);
                pesoIdeal = PesoIdealValor(altura, emailPaciente);

                con.Open();
                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Consulta VALUES(@EmailNutri, @EmailPaciente, @Data, @Peitoral, @IMC,  @PorcentGorduraCorpo," +
                    "@MassaMagra, @MassaCorporal, @Altura, @PorcentGorduraVisceral, @PorcentMuscEsque, @PesoIdeal, @Metabolismo," +
                    "@Abdominal, @Biceps, @CoxaEsq, @CoxaDir, @PanturrilhaEsq, @PanturrilhaDir,  @Subescapular, @Torax, @Triceps, @Ombro," +
                    "@Cintura, @Quadril, @Pescoco,  @Punho, @AntebracoDir, @AntebracoEsq)", con);


                query.Parameters.AddWithValue("@EmailNutri", emailNutri);
                query.Parameters.AddWithValue("@EmailPaciente", emailPaciente);
                query.Parameters.AddWithValue("@IMC", imc);
                query.Parameters.AddWithValue("@Data", data);
                query.Parameters.AddWithValue("@PesoIdeal", pesoIdeal);
                query.Parameters.AddWithValue("@Peitoral", peitoral);
                query.Parameters.AddWithValue("@PorcentGorduraCorpo", porcentGorduraCorpo);
                query.Parameters.AddWithValue("@MassaMagra", massaMagra);
                query.Parameters.AddWithValue("@MassaCorporal", peso);
                query.Parameters.AddWithValue("@PorcentGorduraVisceral", PorcentGorduraVisce);
                query.Parameters.AddWithValue("@Altura", altura);
                query.Parameters.AddWithValue("@PorcentMuscEsque", porcentMuscEsque);
                query.Parameters.AddWithValue("@Metabolismo", metabolismo);
                query.Parameters.AddWithValue("@Abdominal", abdominal);
                query.Parameters.AddWithValue("@Biceps", biceps);
                query.Parameters.AddWithValue("@CoxaEsq", coxaEsq);
                query.Parameters.AddWithValue("@CoxaDir", coxaDir);
                query.Parameters.AddWithValue("@PanturrilhaEsq", panturrilhaEsq);
                query.Parameters.AddWithValue("@PanturrilhaDir", panturrilhaDir);
                query.Parameters.AddWithValue("@Subescapular", subescapular);
                query.Parameters.AddWithValue("@Torax", torax);
                query.Parameters.AddWithValue("@Triceps", triceps);
                query.Parameters.AddWithValue("@Ombro", ombro);
                query.Parameters.AddWithValue("@Cintura", cintura);
                query.Parameters.AddWithValue("@Quadril", quadril);
                query.Parameters.AddWithValue("@Pescoco", pescoco);
                query.Parameters.AddWithValue("@Punho", punho);
                query.Parameters.AddWithValue("@AntebracoDir", antebracoDir);
                query.Parameters.AddWithValue("@AntebracoEsq", antebracoEsq);


                query.ExecuteNonQuery();


            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Consulta cadastrada com sucesso!";
        }


        private double IMC(double altura, double peso)
        {
            double imc;
            try
            {
                imc = peso / (altura * altura);
            }
            catch (Exception ee)
            {
                imc = 0;
            }

            return imc;
        }

        private double PesoIdealValor(double altura, string emailPaciente)
        {
            double pesoIdealValor, tempAltura, temp;
            string sexo = "";


            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa WHERE TipoUsuario = 'Paciente' and Email = @Email", con);
                query.Parameters.AddWithValue("@Email", emailPaciente);

                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        sexo = leitor["Sexo"].ToString();

                    }
                }
                leitor.Close();
                if (sexo.Equals("f"))
                {
                    if (altura > 1.52)
                    {
                        tempAltura = (altura * 100) - 152.4;
                        temp = tempAltura / 2.54;
                        temp = 1.7 * temp;
                        pesoIdealValor = 49 + temp;
                    }
                    else
                    {
                        pesoIdealValor = 49;
                    }
                }
                else
                {
                    if (altura > 1.52)
                    {
                        tempAltura = (altura * 100) - 152.4;
                        temp = altura / 2.54;
                        temp = 2.3 * temp;
                        pesoIdealValor = 50 + temp;
                    }
                    else
                    {
                        pesoIdealValor = 50;
                    }
                }

                con.Close();
            }
            catch (Exception ee)
            {
                pesoIdealValor = 0;
            }

            return pesoIdealValor;
        }


        public static Consulta BuscaConsulta(string email, string dataConsulta, string emailNutri)

        {
            Consulta c = new Consulta();

            try
            {
                con.Open();
                
                SqlCommand query = new SqlCommand("SELECT * FROM Consulta WHERE EmailPaciente = @Email AND EmailNutri = @EmailNutri AND Data = @Data", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@EmailNutri", emailNutri);
                query.Parameters.AddWithValue("@Data", dataConsulta);
                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        c.Data = (DateTime)leitor["Data"];
                        c.Imc = Double.Parse(leitor["IMC"].ToString());
                        c.Peitoral = Double.Parse(leitor["Peitoral"].ToString());
                        c.PorcentGorduraCorpo = Double.Parse(leitor["PorcentGorduraCorpo"].ToString());
                        c.MassaMagra = Double.Parse(leitor["MassaMagra"].ToString());
                        c.Peso = Double.Parse(leitor["MassaCorporal"].ToString());
                        c.Altura = Double.Parse(leitor["Altura"].ToString());
                        c.PorcentGorduraVisce = Double.Parse(leitor["PorcentGorduraVisceral"].ToString());
                        c.PorcentMuscEsque = Double.Parse(leitor["PorcentMuscEsque"].ToString());
                        c.PesoIdeal = Double.Parse(leitor["PesoIdeal"].ToString());
                        c.Metabolismo = leitor["Metabolismo"].ToString();
                        c.Abdominal = Double.Parse(leitor["Abdominal"].ToString());
                        c.Biceps = Double.Parse(leitor["Biceps"].ToString());
                        c.CoxaEsq = Double.Parse(leitor["CoxaEsq"].ToString());
                        c.CoxaDir = Double.Parse(leitor["CoxaDir"].ToString());
                        c.PanturrilhaEsq = Double.Parse(leitor["PanturrilhaEsq"].ToString());
                        c.PanturrilhaDir = Double.Parse(leitor["PanturrilhaDir"].ToString());
                        c.Subescapular = Double.Parse(leitor["Subescapular"].ToString());
                        c.Torax = Double.Parse(leitor["Torax"].ToString());
                        c.Triceps = Double.Parse(leitor["Triceps"].ToString());
                        c.Ombro = Double.Parse(leitor["Ombro"].ToString());
                        c.Cintura = Double.Parse(leitor["Cintura"].ToString());
                        c.Quadril = Double.Parse(leitor["Quadril"].ToString());
                        c.Pescoco = Double.Parse(leitor["Pescoco"].ToString());
                        c.Punho = Double.Parse(leitor["Punho"].ToString());
                        c.AntebracoDir = Double.Parse(leitor["AntebracoDir"].ToString());
                        c.AntebracoEsq = Double.Parse(leitor["AntebracoEsq"].ToString());
                        c.EmailNutri = leitor["EmailNutri"].ToString();
                    }
                }
                else
                {
                    c = null;
                }
            }
            catch (Exception e)
            {
                c = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return c;
        }
    }
}