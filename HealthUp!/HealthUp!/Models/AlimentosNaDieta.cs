﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class AlimentosNaDieta
    {
        private string horarioAlimento, nomeAlimento;
        private int qtdAlimento, idAlimento;


        public string HorarioAlimento
        {
            get
            {
                return horarioAlimento;
            }

            set
            {
                horarioAlimento = value;
            }
        }

        public int IdAlimento
        {
            get
            {
                return idAlimento;
            }

            set
            {
                idAlimento = value;
            }
        }

        public string NomeAlimento
        {
            get
            {
                return nomeAlimento;
            }

            set
            {
                nomeAlimento = value;
            }
        }

        public int QtdAlimento
        {
            get
            {
                return qtdAlimento;
            }

            set
            {
                qtdAlimento = value;
            }
        }
    }
}