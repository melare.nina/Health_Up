﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Pessoa
    {
        private string email, senha = "paciente123", tipoUsu = "Paciente", nome, telefone, sexo;
        private DateTime dataNasc, ultimaConsulta;
        private List<Consulta> consultas = new List<Consulta>();

        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        private static SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public List<Consulta> Consultas
        {
            get { return consultas; }
            set
            { consultas = value; }
        }

        public string Email
        {
            get { return email; }
            set
            { email = value; }
        }

        public string Telefone
        {
            get { return telefone; }
            set { telefone = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        public DateTime UltimaConsulta
        {
            get { return ultimaConsulta; }
            set { ultimaConsulta = value; }
        }

        public string TipoUsu
        {
            get { return tipoUsu; }
            set { tipoUsu = value; }
        }

        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        public DateTime DataNasc
        {
            get { return dataNasc; }
            set { dataNasc = value; }
        }


        public string Cadastrar()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Pessoa VALUES(@Email, @Nome,  @CRN, @DataDeNascimento, @Senha, @Sexo, @TipoUsuario)", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@CRN", 0000);
                query.Parameters.AddWithValue("@DataDeNascimento", dataNasc);
                query.Parameters.AddWithValue("@Senha", senha);
                query.Parameters.AddWithValue("@Sexo", sexo);
                query.Parameters.AddWithValue("@TipoUsuario", tipoUsu);

                query.ExecuteNonQuery();

                query = new SqlCommand("INSERT INTO Telefone VALUES(@Numero, @Email)", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Numero", telefone);
                query.ExecuteNonQuery();


            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Paciente cadastrado com sucesso!";
        }


        public static List<Pessoa> ListaPaciente(string nome)
        {
            List<Pessoa> lista = new List<Pessoa>();
            try
            {
                con.Open();
                con2.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa WHERE TipoUsuario = 'Paciente'", con);
                if (nome != "")
                {
                    query.CommandText = query.CommandText + "and Nome LIKE @Nome";
                    query.Parameters.AddWithValue("@Nome", "%"+nome+"%");

                }

                SqlDataReader leitor = query.ExecuteReader();


                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        Pessoa p = new Pessoa();
                        p.Email = leitor["Email"].ToString();
                        p.Nome = leitor["Nome"].ToString();
                        p.DataNasc = (DateTime)leitor["DataDeNascimento"];
                        p.Sexo = leitor["Sexo"].ToString();



                        SqlCommand query2 = new SqlCommand("SELECT * FROM Telefone WHERE Email = @Email", con2);
                        query2.Parameters.AddWithValue("@Email", p.Email);
                        SqlDataReader leitor2 = query2.ExecuteReader();

                        while (leitor2.Read())
                        {
                            p.Telefone = leitor2["Numero"].ToString();
                        }

                        lista.Add(p);
                        leitor2.Close();

                    }
                }
                else
                {
                    lista = null;
                }
            }
            catch (Exception e)
            {
                lista = new List<Pessoa>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();
            if (con2.State == ConnectionState.Open)
                con2.Close();

            return lista;
        }

        internal string Editar()
        {
            string res = "Paciente editado com sucesso!";
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("UPDATE Pessoa SET Email = @Email, Nome = @Nome, DataDeNascimento = @DataDeNascimento, Sexo = @Sexo Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@Sexo", sexo);
                query.Parameters.AddWithValue("@DataDeNascimento", dataNasc);
                query.ExecuteNonQuery();

                query = new SqlCommand("UPDATE Telefone SET Numero = @Numero Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Numero", telefone);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }


        public string TrocaSenha(string senha)
        {
            string res = "Senha alterada com sucesso!";
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("UPDATE Pessoa SET Senha = @Senha Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Senha", senha);
                query.ExecuteNonQuery();
                
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }


        internal string Excluir()
        {
            string res = "Paciente deletado com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar = new SqlCommand("DELETE FROM Telefone WHERE Email = @Email", con);
                deletar.Parameters.AddWithValue("@Email", email);
                deletar.ExecuteNonQuery();

                deletar =
                new SqlCommand("DELETE FROM Pessoa WHERE Email = @Email", con);
                deletar.Parameters.AddWithValue("@Email", email);
                deletar.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }



        public static Pessoa BuscaPaciente(string email)

        {
            Pessoa p = new Pessoa();
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa p, Telefone t  WHERE p.Email = t.Email AND p.Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);

                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    email = leitor["Email"].ToString();
                    p.Email = leitor["Email"].ToString();
                    p.Nome = leitor["Nome"].ToString();
                    p.DataNasc = (DateTime)leitor["DataDeNascimento"];
                    p.Sexo = leitor["Sexo"].ToString();
                    p.Telefone = leitor["Numero"].ToString();
                }

                leitor.Close();

                query = new SqlCommand("SELECT * FROM Consulta WHERE EmailPaciente = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        Consulta c = new Consulta();

                        c.Data = (DateTime)leitor["Data"];
                        c.Imc = Double.Parse(leitor["IMC"].ToString());
                        c.Peitoral = Double.Parse(leitor["Peitoral"].ToString());
                        c.PorcentGorduraCorpo = Double.Parse(leitor["PorcentGorduraCorpo"].ToString());
                        c.MassaMagra = Double.Parse(leitor["MassaMagra"].ToString());
                        c.Peso = Double.Parse(leitor["MassaCorporal"].ToString());
                        c.Altura = Double.Parse(leitor["Altura"].ToString());
                        c.PorcentGorduraVisce = Double.Parse(leitor["PorcentGorduraVisceral"].ToString());
                        c.PorcentMuscEsque = Double.Parse(leitor["PorcentMuscEsque"].ToString());
                        c.PesoIdeal = Double.Parse(leitor["PesoIdeal"].ToString());
                        c.Metabolismo = leitor["Metabolismo"].ToString();
                        c.Abdominal = Double.Parse(leitor["Abdominal"].ToString());
                        c.Biceps = Double.Parse(leitor["Biceps"].ToString());
                        c.CoxaEsq = Double.Parse(leitor["CoxaEsq"].ToString());
                        c.CoxaDir = Double.Parse(leitor["CoxaDir"].ToString());
                        c.PanturrilhaEsq = Double.Parse(leitor["PanturrilhaEsq"].ToString());
                        c.PanturrilhaDir = Double.Parse(leitor["PanturrilhaDir"].ToString());
                        c.Subescapular = Double.Parse(leitor["Subescapular"].ToString());
                        c.Torax = Double.Parse(leitor["Torax"].ToString());
                        c.Triceps = Double.Parse(leitor["Triceps"].ToString());
                        c.Ombro = Double.Parse(leitor["Ombro"].ToString());
                        c.Cintura = Double.Parse(leitor["Cintura"].ToString());
                        c.Quadril = Double.Parse(leitor["Quadril"].ToString());
                        c.Pescoco = Double.Parse(leitor["Pescoco"].ToString());
                        c.Punho = Double.Parse(leitor["Punho"].ToString());
                        c.AntebracoDir = Double.Parse(leitor["AntebracoDir"].ToString());
                        c.AntebracoEsq = Double.Parse(leitor["AntebracoEsq"].ToString());
                        c.EmailNutri = leitor["EmailNutri"].ToString();
                        p.consultas.Add(c);
                    }
                }
                else
                {
                    p.consultas = null;
                }
            }
            catch (Exception e)
            {
                p = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return p;
        }


    }
}