﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace HealthUp_.Models
{
    public class GerarPDF : TNEReport
    {
        private string email;
        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public GerarPDF(int id, string emailNutri, string emailPaciente)
        {
            Email = emailPaciente;
            Id = id;
            EmailNutri = emailNutri;
            Paisagem = true;
        }




        public override void MontaCorpoDados()
        {
            base.MontaCorpoDados();

            #region Cabeçalho do Relatório
            PdfPTable table = new PdfPTable(3);
            BaseColor preto = new BaseColor(0, 0, 0);
            BaseColor fundo = new BaseColor(109, 224, 138);
            BaseColor branco = new BaseColor(255, 255, 255);

            Font font = FontFactory.GetFont("Verdana", 12, Font.NORMAL, preto);
            Font titulo = FontFactory.GetFont("Verdana", 14, Font.BOLD, branco);

            float[] colsW = { 10, 10, 10};
            table.SetWidths(colsW);
            table.HeaderRows = 1;
            table.WidthPercentage = 100f;

            table.DefaultCell.Border = PdfPCell.BOTTOM_BORDER;
            table.DefaultCell.BorderColor = preto;
            table.DefaultCell.BorderColorBottom = new BaseColor(255, 255, 255);
            table.DefaultCell.Padding = 10;
            
            table.AddCell(getNewCell("Alimento", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Quantidade", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Horário", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));

            #endregion

            Dieta d = Dieta.BuscaDieta(email, Id);

            for (int i = 0; i < d.AlimentosDieta.Count; i++)
            {
                table.AddCell(getNewCell(d.AlimentosDieta[i].NomeAlimento.ToString(), font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(d.AlimentosDieta[i].QtdAlimento.ToString(), font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(d.AlimentosDieta[i].HorarioAlimento.ToString(), font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
            }

            doc.Add(table);
        }
    }
}
