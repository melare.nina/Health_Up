﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Nutri : Pessoa
    {
        private string senhaNutri = "nutri123";
        private int crn;

        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        private static SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
       

   

        public string SenhaNutri
        {
            get { return senhaNutri; }
            set { senhaNutri = value; }
        }


        public int Crn
        {
            get
            {
                return crn;
            }

            set
            {
                crn = value;
            }
        }

        new public string Cadastrar()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos                                     //comandos usando o @ para ficar mais seguro                          //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Pessoa VALUES(@Email, @Nome,  @CRN, @DataDeNascimento, @Senha, @Sexo, @TipoUsuario)", con);
                query.Parameters.AddWithValue("@Email", Email);
                query.Parameters.AddWithValue("@Nome", Nome);
                query.Parameters.AddWithValue("@CRN", crn);
                query.Parameters.AddWithValue("@DataDeNascimento", DataNasc);
                query.Parameters.AddWithValue("@Senha", senhaNutri);
                query.Parameters.AddWithValue("@Sexo", Sexo);
                query.Parameters.AddWithValue("@TipoUsuario", "Nutricionista");

                query.ExecuteNonQuery();

                query = new SqlCommand("INSERT INTO Telefone VALUES(@Numero, @Email)", con);
                query.Parameters.AddWithValue("@Email", Email);
                query.Parameters.AddWithValue("@Numero", Telefone);

                query.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Nutricionista cadastrado com sucesso!";
        }


        public static List<Nutri> ListaNutricionistas(string nome)
        {
            List<Nutri> lista = new List<Nutri>();
            try
            {
                con.Open();
                con2.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa WHERE TipoUsuario = 'Nutricionista'", con);
                if (nome != "")
                {
                    query.CommandText = query.CommandText + "and Nome LIKE @Nome";
                    query.Parameters.AddWithValue("@Nome", "%" + nome + "%");

                }

                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        Nutri n = new Nutri();
                        n.Email = leitor["Email"].ToString();
                        n.Nome = leitor["Nome"].ToString();
                        n.Crn = int.Parse(leitor["CRN"].ToString());
                        n.DataNasc = (DateTime)leitor["DataDeNascimento"];
                        n.Sexo = leitor["Sexo"].ToString();

                        

                        SqlCommand query2 = new SqlCommand("SELECT * FROM Telefone WHERE Email = @Email", con2);
                        query2.Parameters.AddWithValue("@Email", n.Email);
                        SqlDataReader leitor2 = query2.ExecuteReader();

                        while (leitor2.Read())
                        {
                            n.Telefone = leitor2["Numero"].ToString();
                        }

                        lista.Add(n);
                        leitor2.Close();
                        
                    }

                    
                }
                else
                {
                    lista = null;
                }
            }
            catch (Exception e)
            {
                lista = new List<Nutri>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();
            if (con2.State == ConnectionState.Open)
                con2.Close();

            return lista;
        }

        new internal string Editar()
        {
            string res = "Nutricionista editado com sucesso!";
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("UPDATE Pessoa SET Email = @Email, Nome = @Nome, CRN = @CRN, DataDeNascimento = @DataDeNascimento, Sexo = @Sexo Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", Email);
                query.Parameters.AddWithValue("@Nome", Nome);
                query.Parameters.AddWithValue("@Sexo", Sexo);
                query.Parameters.AddWithValue("@CRN", crn);
                query.Parameters.AddWithValue("@DataDeNascimento", DataNasc);
                query.ExecuteNonQuery();

                query = new SqlCommand("UPDATE Telefone SET Numero = @Numero Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", Email);
                query.Parameters.AddWithValue("@Numero", Telefone);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        new internal string Excluir()
        {
            string res = "Nutricionista deletado com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar = new SqlCommand("DELETE FROM Telefone WHERE Email = @Email", con);
                deletar.Parameters.AddWithValue("@Email", Email);
                deletar.ExecuteNonQuery();

                deletar =
                new SqlCommand("DELETE FROM Pessoa WHERE Email = @Email", con);
                deletar.Parameters.AddWithValue("@Email", Email);
                deletar.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }



        public static Nutri BuscaNutricionista(string email)

        {
            Nutri n = new Nutri();
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Pessoa p, Telefone t  WHERE p.Email = t.Email AND p.Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);

                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    n.Email = leitor["Email"].ToString();
                    n.Nome = leitor["Nome"].ToString();
                    n.Crn = int.Parse(leitor["CRN"].ToString());
                    n.DataNasc = (DateTime)leitor["DataDeNascimento"];
                    n.Sexo = leitor["Sexo"].ToString();
                    n.Telefone = leitor["Numero"].ToString();
                }

                leitor.Close();

            }
            catch (Exception e)
            {
                n = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return n;
        }


    }
}