﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Alimentos
    {
        string nome;
        double proteina, calcio, gordura, kcal;
        int idAlimento;

        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public double Proteina
        {
            get { return proteina; }
            set { proteina = value; }
        }

        public double Calcio
        {
            get { return calcio; }
            set { calcio = value; }
        }
        public double Gordura
        {
            get { return gordura; }
            set { gordura = value; }
        }
        public double Kcal
        {
            get { return kcal; }
            set { kcal = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public int IdAlimento
        {
            get
            {
                return idAlimento;
            }

            set
            {
                idAlimento = value;
            }
        }

        public string CadastrarAlimento()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Alimentos VALUES(@Nome, @Proteina,  @Calcio, @Gordura, @Kcal)", con);
                query.Parameters.AddWithValue("@Proteina", proteina);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@Calcio", calcio);
                query.Parameters.AddWithValue("@Gordura", gordura);
                query.Parameters.AddWithValue("@Kcal", kcal);

                query.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Alimento cadastrado com sucesso!";
        }

        public static List<Alimentos> ListaAlimentos(string nome)
        {
            List<Alimentos> lista = new List<Alimentos>();
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Alimentos ", con);
                if (nome != "")
                {
                    query.CommandText = query.CommandText + " WHERE Nome = @Nome";
                    query.Parameters.AddWithValue("@Nome", nome);
                }

                query.CommandText = query.CommandText + " ORDER BY Nome ASC";

                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        Alimentos a = new Alimentos();
                        a.Nome = leitor["Nome"].ToString();
                        a.Proteina = double.Parse(leitor["Proteina"].ToString());
                        a.Gordura = double.Parse(leitor["Gordura"].ToString());
                        a.Calcio = double.Parse(leitor["Calcio"].ToString());
                        a.Kcal = double.Parse(leitor["Kcal"].ToString());
                        a.IdAlimento = int.Parse(leitor["IdAlimento"].ToString());

                        lista.Add(a);
                    }
                }
                else
                {
                    lista = null;
                }
            }
            catch (Exception e)
            {
                lista = new List<Alimentos>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        internal string Editar(string nomeOriginal)
        {
            string res = "Alimento editado com sucesso!";
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("UPDATE Alimentos SET Nome = @Nome, Proteina = @Proteina, Calcio = @Calcio, Gordura = @Gordura, Kcal = @Kcal Where Nome = @NomeOrigninal", con);
                query.Parameters.AddWithValue("@Proteina", proteina);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@NomeOrigninal", nomeOriginal);
                query.Parameters.AddWithValue("@Calcio", calcio);
                query.Parameters.AddWithValue("@Gordura", gordura);
                query.Parameters.AddWithValue("@Kcal", kcal);
                query.ExecuteNonQuery();
                
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        public static Alimentos BuscaAlimento(string nome)
        {
            Alimentos a = new Alimentos();
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Alimentos WHERE Nome = @Nome", con);
                query.Parameters.AddWithValue("@Nome", nome);

                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    a.Nome = leitor["Nome"].ToString();
                    a.Proteina = double.Parse(leitor["Proteina"].ToString());
                    a.Gordura = double.Parse(leitor["Gordura"].ToString());
                    a.Calcio = double.Parse(leitor["Calcio"].ToString());
                    a.Kcal = double.Parse(leitor["Kcal"].ToString());
                }

                leitor.Close();
            }
            catch (Exception e)
            {
                a = new Alimentos();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return a;
        }



        internal string Excluir()
        {
            string res = "Alimento deletado com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar = new SqlCommand("DELETE FROM Alimentos WHERE Nome = @Nome", con);
                deletar.Parameters.AddWithValue("@Nome", nome);
                deletar.ExecuteNonQuery();
                

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }


    }
}