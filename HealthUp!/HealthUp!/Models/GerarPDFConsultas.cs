﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace HealthUp_.Models
{
    public class GerarPDFConsultas : TNEReportConsultas
    {
        private string email, dataConsulta;
        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string DataConsulta
        {
            get
            {
                return dataConsulta;
            }

            set
            {
                dataConsulta = value;
            }
        }

        public GerarPDFConsultas(string emailNutri, string emailPaciente, string dataConsulta)
        {
            Email = emailPaciente;
            EmailNutri = emailNutri;
            DataConsulta = dataConsulta;
            Paisagem = true;
        }




        public override void MontaCorpoDados()
        {
            base.MontaCorpoDados();

            #region Cabeçalho do Relatório
            PdfPTable table = new PdfPTable(10);
            BaseColor preto = new BaseColor(0, 0, 0);
            BaseColor fundo = new BaseColor(109, 224, 138);
            BaseColor branco = new BaseColor(255, 255, 255);

            Font font = FontFactory.GetFont("Verdana", 8, Font.NORMAL, preto);
            Font titulo = FontFactory.GetFont("Verdana", 9, Font.BOLD, branco);

            float[] colsW = { 250, 300, 300, 300, 300, 300, 300, 300, 300, 300 };
            table.SetWidths(colsW);
            table.HeaderRows = 1;
            table.WidthPercentage = 100f;
            

            table.DefaultCell.Border = PdfPCell.BOTTOM_BORDER;
            table.DefaultCell.BorderColor = preto;
            table.DefaultCell.BorderColorBottom = new BaseColor(255, 255, 255);
            table.DefaultCell.Padding = 10;

            table.AddCell(getNewCell("Imc", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Porcent. Gordura Corporal", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Porcent. Massa Magra", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Peso", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Altura", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Porcent. Gordura Visceral", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Porcent. Músculos Esqueléticos", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Peso Ideal", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Metabolismo", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
            table.AddCell(getNewCell("Abdominal", titulo, Element.ALIGN_CENTER, 10, PdfPCell.BOTTOM_BORDER, preto, fundo));
           

            #endregion

            Consulta c = Consulta.BuscaConsulta(Email, DataConsulta, EmailNutri);
            
                table.AddCell(getNewCell(c.Imc.ToString().Substring(0,5) + "Kg/m²", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.PorcentGorduraCorpo.ToString() + "%", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.MassaMagra.ToString() + "%", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.Peso.ToString() + "kg", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.Altura.ToString() + "m", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.PorcentGorduraVisce.ToString() + "%", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.PorcentMuscEsque.ToString() + "%", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.PesoIdeal.ToString().Substring(0, 5) + "kg", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.Metabolismo.ToString(), font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));
                table.AddCell(getNewCell(c.Abdominal.ToString() + "cm", font, Element.ALIGN_CENTER, 5, PdfPCell.BOTTOM_BORDER));

            doc.Add(table);
        }
    }
}
