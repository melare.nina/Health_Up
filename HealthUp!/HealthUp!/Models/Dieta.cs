﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HealthUp_.Models
{
    public class Dieta
    {
        private string[] horarioAlimento = new string[12];
        private string emailNutri, emailPaciente;
        private int[] quantidadeAlimento = new int[12], idAlimento = new int[12];
        private int numDieta;
        private DateTime dataDieta;
        private List<AlimentosNaDieta> alimentosDieta = new List<AlimentosNaDieta>();


        private static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        private static SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public DateTime DataDieta
        {
            get { return dataDieta; }
            set { dataDieta = value; }
        }

        public string[] HorarioAlimento
        {
            get { return horarioAlimento; }
            set { horarioAlimento = value; }
        }

        public int[] IdAlimento
        {
            get { return idAlimento; }
            set { idAlimento = value; }
        }
        public string EmailNutri
        {
            get { return emailNutri; }
            set { emailNutri = value; }
        }
        public string EmailPaciente
        {
            get { return emailPaciente; }
            set { emailPaciente = value; }
        }

        public int NumDieta
        {
            get { return numDieta; }
            set { numDieta = value; }
        }
        public int[] QuantidadeAlimento
        {
            get { return quantidadeAlimento; }
            set { quantidadeAlimento = value; }
        }

        public List<AlimentosNaDieta> AlimentosDieta
        {
            get
            {
                return alimentosDieta;
            }

            set
            {
                alimentosDieta = value;
            }
        }

        public string CadastrarDieta()
        {

            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Dieta WHERE EmailPaciente = @EmailPaciente", con);
                query.Parameters.AddWithValue("@EmailPaciente", emailPaciente);
                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        numDieta = (int)leitor["NumDieta"] + 1;
                    }

                }
                else
                {
                    numDieta = 1;
                }

                leitor.Close();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                query = new SqlCommand("INSERT INTO Dieta VALUES(@NumDieta, @EmailNutri,  @EmailPaciente, @DataDieta)", con);
                query.Parameters.AddWithValue("@NumDieta", numDieta);
                query.Parameters.AddWithValue("@EmailNutri", emailNutri);
                query.Parameters.AddWithValue("@EmailPaciente", emailPaciente);
                query.Parameters.AddWithValue("@DataDieta", DateTime.Today);
                query.ExecuteNonQuery();

                for (int i = 0; i <= 11; i++)
                {
                    query = new SqlCommand("INSERT INTO Dieta_Contem_Alimento VALUES(@Horario, @IdAlimento, @NumDieta, @EmailNutri,  @EmailPaciente, @QtdAlimento)", con);
                    query.Parameters.AddWithValue("@Horario", horarioAlimento[i]);
                    query.Parameters.AddWithValue("@IdAlimento", idAlimento[i]);
                    query.Parameters.AddWithValue("@NumDieta", numDieta);
                    query.Parameters.AddWithValue("@EmailNutri", emailNutri);
                    query.Parameters.AddWithValue("@EmailPaciente", emailPaciente);
                    query.Parameters.AddWithValue("@QtdAlimento", quantidadeAlimento[i]);
                    query.ExecuteNonQuery();
                }

            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Dieta cadastrada com sucesso!";
        }


        public static List<Dieta> ListaDietas(string email)
        {
            List<Dieta> lista = new List<Dieta>();
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * FROM Dieta WHERE EmailPaciente = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                SqlDataReader leitor = query.ExecuteReader();


                con2.Open();

                if (leitor.HasRows)
                {

                    while (leitor.Read())
                    {
                        Dieta d = new Dieta();
                        d.NumDieta = (int)leitor["NumDieta"];
                        d.DataDieta = (DateTime)leitor["DataDieta"];
                        d.EmailPaciente = leitor["EmailPaciente"].ToString();
                        d.EmailNutri = leitor["EmailNutri"].ToString();


                        SqlCommand query2 = new SqlCommand("SELECT * FROM Dieta_Contem_Alimento d, Alimentos a WHERE d.IdAlimento = a.IdAlimento AND d.EmailPaciente = @Email AND d.NumDieta = @NumDieta", con2);
                        query2.Parameters.AddWithValue("@Email", email);
                        query2.Parameters.AddWithValue("@NumDieta", d.NumDieta);
                        SqlDataReader leitor2 = query2.ExecuteReader();

                        if (leitor2.HasRows)
                        {
                            while (leitor2.Read())
                            {
                                AlimentosNaDieta alimentos = new AlimentosNaDieta();
                                alimentos.HorarioAlimento = leitor2["Horario"].ToString();
                                alimentos.IdAlimento = (int)leitor2["IdAlimento"];
                                alimentos.QtdAlimento = (int)leitor2["QtdAlimento"];
                                alimentos.NomeAlimento = leitor2["Nome"].ToString();

                                d.alimentosDieta.Add(alimentos);

                            }

                        }
                        else
                        {
                            lista = null;
                        }
                        leitor2.Close();
                        lista.Add(d);
                    }
                }
                else
                {
                    lista = null;
                }
            }
            catch (Exception e)
            {
                lista = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            if (con2.State == ConnectionState.Open)
                con2.Close();

            return lista;
        }

        public static Dieta BuscaDieta(string email, int numDieta)
        {
            Dieta d = new Dieta();
            try
            {
                con2.Open();


                SqlCommand query2 = new SqlCommand("SELECT * FROM Dieta_Contem_Alimento d, Alimentos a WHERE d.IdAlimento = a.IdAlimento AND d.EmailPaciente = @Email AND d.NumDieta = @NumDieta", con2);
                query2.Parameters.AddWithValue("@Email", email);
                query2.Parameters.AddWithValue("@NumDieta", numDieta);
                SqlDataReader leitor2 = query2.ExecuteReader();

                if (leitor2.HasRows)
                {
                    while (leitor2.Read())
                    {
                        AlimentosNaDieta alimentos = new AlimentosNaDieta();
                        alimentos.HorarioAlimento = leitor2["Horario"].ToString();
                        alimentos.IdAlimento = (int)leitor2["IdAlimento"];
                        alimentos.QtdAlimento = (int)leitor2["QtdAlimento"];
                        alimentos.NomeAlimento = leitor2["Nome"].ToString();

                        d.alimentosDieta.Add(alimentos);

                    }
                }
                else
                {
                    d.alimentosDieta = null;
                }

                leitor2.Close();

            }
            catch (Exception e)
            {
                d = null;
            }

            if (con2.State == ConnectionState.Open)
                con2.Close();

            return d;
        }


    }
}